package taller5GORM

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FileController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond File.list(params), model:[FileInstanceCount: File.count()]
    }

    def show(File FileInstance) {
        respond FileInstance
    }

    def create() {
        def theFile = request.getFile(params)
		if(theFile.size.isLong() &&	(theFile.fileType == ".txt" || theFile.fileType == ".png") ){
			respond new File(params)
		}
    }

    @Transactional
    def save(File FileInstance) {
        if (FileInstance == null) {
            notFound()
            return
        }

        if (FileInstance.hasErrors()) {
            respond FileInstance.errors, view:'create'
            return
        }

        FileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'File.label', default: 'File'), FileInstance.id])
                redirect FileInstance
            }
            '*' { respond FileInstance, [status: CREATED] }
        }
    }

    def edit(File FileInstance) {
        respond FileInstance
    }
	def beforeInterceptor = {
		println "Se va a ejecutar la accion:  ${actionUri}"
	}
	
	def afterInterceptor = { model ->
		println " *** Se acaba de ejecutar la accion:  ${actionUri}"
	}

    @Transactional
    def update(File FileInstance) {
        if (FileInstance == null) {
            notFound()
            return
        }

        if (FileInstance.hasErrors()) {
            respond FileInstance.errors, view:'edit'
            return
        }

        FileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'File.label', default: 'File'), FileInstance.id])
                redirect FileInstance
            }
            '*'{ respond FileInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(File FileInstance) {

        if (FileInstance == null) {
            notFound()
            return
        }

        FileInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'File.label', default: 'File'), FileInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'File.label', default: 'File'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	def download = {
		def file = new File("").getAbsolutePath()
		
		if (file.exists()) {
		   response.setContentType(params.fileType)
		   response.setHeader("Content-disposition", "filename=${params.actualFile}")
		   response.outputStream << file.content
		   return
		}
	}
	
	def share = {
		render "No se a implementado esta funcionalidad"
	}
}

package taller5GORM

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdminController {
	    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Admin.list(params), model:[AdminInstanceCount: Admin.count()]
    }

    def show(Admin AdminInstance) {
        respond AdminInstance
    }

    def create() {
        respond new Admin(params)
    }

    @Transactional
    def save(Admin AdminInstance) {
        if (AdminInstance == null) {
            notFound()
            return
        }

        if (AdminInstance.hasErrors()) {
            respond AdminInstance.errors, view:'create'
            return
        }

        AdminInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'Admin.label', default: 'Admin'), AdminInstance.id])
                redirect AdminInstance
            }
            '*' { respond AdminInstance, [status: CREATED] }
        }
    }

    def edit(Admin AdminInstance) {
        respond AdminInstance
    }

	def beforeInterceptor = {
		println "Se va a ejecutar la accion:  ${actionUri}"
	}
	
	def afterInterceptor = { model ->
		println "*** Se acaba de ejecutar la accion:  ${actionUri}"
	}
	
    @Transactional
    def update(Admin AdminInstance) {
        if (AdminInstance == null) {
            notFound()
            return
        }

        if (AdminInstance.hasErrors()) {
            respond AdminInstance.errors, view:'edit'
            return
        }

        AdminInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Admin.label', default: 'Admin'), AdminInstance.id])
                redirect AdminInstance
            }
            '*'{ respond AdminInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Admin AdminInstance) {

        if (AdminInstance == null) {
            notFound()
            return
        }

        AdminInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Admin.label', default: 'Admin'), AdminInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}

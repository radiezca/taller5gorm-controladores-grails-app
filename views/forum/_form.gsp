<%@ page import="taller5GORM.Forum" %>



<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="forum.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="20" required="" value="${forumInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'category', 'error')} required">
	<label for="category">
		<g:message code="forum.category.label" default="Category" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="category" maxlength="15" required="" value="${forumInstance?.category}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'admin', 'error')} required">
	<label for="admin">
		<g:message code="forum.admin.label" default="Admin" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="admin" name="admin.id" from="${taller5GORM.Admin.list()}" optionKey="id" required="" value="${forumInstance?.admin?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'post', 'error')} ">
	<label for="post">
		<g:message code="forum.post.label" default="Post" />
		
	</label>
	<g:select name="post" from="${taller5GORM.Post.list()}" multiple="multiple" optionKey="id" size="5" value="${forumInstance?.post*.id}" class="many-to-many"/>

</div>


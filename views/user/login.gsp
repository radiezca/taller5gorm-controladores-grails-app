<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="login"/>
		<title>Login</title>
		<style type="text/css" media="screen">
			body{
				background-color: #000;
			}
			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}
			.center {
			    margin-left: auto;
			    margin-right: auto;
			    width: 70%;
			    background-color: #b0e0e6;
			}

		</style>
	</head>
	<body>
		<div id="grailsLogo" role="banner"><asset:image src="edutainmet.jpg"/></div>
	</body>
</html>
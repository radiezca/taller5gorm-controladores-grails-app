package taller5GORM

class Admin extends User{
	int level
	double rating
	
	static hasMany	 = [forum:Forum]

	/*Lo cambie a true ya que como no hay subtablas de herencia puede haber valores nullos */
	static constraints = {
		level nullable: true, range: 1..5
		rating nullable: true, range: 0..100
	}
	
	static mapping = {
		sort level: "desc" 
	}
}

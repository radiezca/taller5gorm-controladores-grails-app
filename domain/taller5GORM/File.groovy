package taller5GORM

class File {

    String fileType
	Byte[]	content
	Long	 size

	static belongsTo =[post:Post]

	static constraints = {
		fileType blank:false, nullable:false, matches:"[a-zA-Z]*/[a-zA-Z]*"
		content blank:false, nullable:false
		size blank:false, nullable:false, range: 0..10485760
	}
}
package taller5GORM

import java.util.Date;

class Forum {

	//Segun el UML y el primer taller category es de tipo String no int
    String name
	Date dateCreated
	String category

	static belongsTo = [admin:Admin]
	static hasMany = [post:Post]
	/* debido a que en Post existe la relaci�n belongsTo los 
		post se borran al tiempo que se borra un foro*/

	static constraints = {
		name nullable: false, size: 3..20, unique: true
		dateCreated nullable: false, min: new Date()
		category nullable: false, size:3..15
	}
	
	static mapping = {
		post column : 'fatherForum_id'
	}
	
	//Antes de insertar guarda la fecha de creaci�n
	def beforeInsert() {
	    dateCreated = new Date()
	}
}

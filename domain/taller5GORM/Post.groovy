package taller5GORM

import java.util.Date;

class Post {

    String topic
	Date dateCreated
	Date lastUpdate
	boolean	itsAllowed

	static belongTo = [regular:Regular,forum:Forum]
	static hasMany = [file:File]

	static constraints = {
		topic nullable:false, size:3..50
		dateCreated nullable:false, min: new Date()
		lastUpdate nullable:false, min: new Date()
		itsAllowed nullable:false
	}
	
	static mapping = {
		topic column: 'post_topic'
		dateCreated column: 'post_date_created'
		lastUpdate column: 'post_last_update'
		itsAllowed column: 'post_its_allowed'
				
		file column: 'post_belongs_id'
		file cascade: "all-delete-orphan"
	}	
	
	def comment = {}
	
	def rate = {}
	
	def share = {}
	
	//Esto se justifica por la misma raz�n de la clase Forum	
	//Antes de insertar, asignar la fecha actual
	def beforeInsert() {
		dateCreated = new Date()
	}
	
	//Antes de actualizar, asignar la fecha actual
	def beforeUpdate() {
		lastUpdate = new Date()
	}
	
}

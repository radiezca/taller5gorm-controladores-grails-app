package taller5GORM

class Regular extends User{

    int postViews
	int strikesNumber
	int starsNumber

	static hasMany = [post:Post]
	
	/* "all-delete-orphan" --> Applies only to one-to-many associations and
	 * indicates that when a child is removed from an association then it
	 * should be automatically deleted. Children are also deleted when the
	 * parent is.*/
	static mapping = {
		post cascade: "all-delete-orphan"
	}

	/*Lo cambie a true ya que como no hay subtablas de herencia puede haber valores nulos */
	static constraints = {
		postViews nullable:true, defaultValue: 0
		strikesNumber nullable:true, range: 0..3
		starsNumber nullable:true, range: 0..5		
	}
}